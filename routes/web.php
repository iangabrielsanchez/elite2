<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'api/v1'], function () {
    Route::get('/dogs-registrations', 'DogsController@index');
    Route::get('/dogs-registrations/{dog}', 'DogsController@search');
});
//Route::get('/dogs-registrations')

/*
GET /dog-registrations - fetches 50 dog registrations
GET /dog-registrations?limit=N
GET /dog-registrations?skip=N
GET /dog-registrations?order=field ASC|DESC
GET /dog-registrations?limit=..&skip=..&order=..
GET /dog-registrations/801 - fetches dog registration for id 801 (Reference = 801)
GET /dog-registrations/9999 - no matching dog registration for id 9999 returns a HTTP 404 with JSON content:
GET /dog-registrations?skip=-2 - negative skip returns HTTP 400 with JSON content:
GET /dog-registrations?limit=-2 - negative limit returns HTTP 400 with JSON content:
*/
