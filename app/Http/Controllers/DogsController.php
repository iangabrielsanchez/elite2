<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Dog;

class DogsController extends Controller
{
    /*
    TODO:
    GET /dog-registrations?order=field ASC|DESC
    GET /dog-registrations?limit=..&skip=..&order=..
    GET /dog-registrations/801 - fetches dog registration for id 801 (Reference = 801)
    GET /dog-registrations/9999 - no matching dog registration for id 9999 returns a HTTP 404 with JSON content:
    GET /dog-registrations?skip=-2 - negative skip returns HTTP 400 with JSON content:
    GET /dog-registrations?limit=-2 - negative limit returns HTTP 400 with JSON content:

    */
    public function index(Request $request)
    {
        $xml2014 = simplexml_load_file('xml/dog-registrations-2014.xml');
        $json2014 = json_encode($xml2014);
        $arr2014 = json_decode($json2014, true);

        $xml2015 = simplexml_load_file('xml/dog-registrations-2015.xml');
        $json2015 = json_encode($xml2015);
        $arr2015 = json_decode($json2015, true);

        //dd($request);

        $start = 0;
        $limit = 50;
        $skip = 0;
        if($request->limit!=null){
            $limit = $request->limit;
            if($request->limit<0){
                return response()->json([
                    "error" => array('status' => 400, 'message' => 'limit is negative')
                ], 400);
            }
        }
        if(!is_numeric($request->limit)){
            $limit = 50;
        }


        if($request->skip!=null){
            $skip = $request->skip;
            if($request->skip<0){
                return response()->json([
                    "error" => array('status' => 400, 'message' => 'number to skip cannot be negative')
                ], 400);
            }
            if(!is_numeric($request->skip)){
                $skip = 0;
            }
        }




        $ret = array();
        $retc = 0;


        //var_dump($arr2015);
        $arrs = array();
        $arrs[0] = $arr2014;
        $arrs[1] = $arr2015;
        //dd($arr2014);
        //print_r($arrs[0]);

        //$arrs = array_merge($arr2014, $arr2015);
        //dd($arr2014);


        $start = $skip;
        $end = $skip+$limit;
        for ($i=$start; $i < $end; $i++) {
            $y = 0;
            $id = $i;
            $yr = 2014;
            if($i<1576){
                $y = 0;
                $yr = 2014;
            }
            else{
                $y=1;
                $id = $i-1576;
                $yr = 2015;
            }
            //echo "y is $y and id is $id and yr is $yr"; echo "<br />";

            try{
                $ret[$i-$start] = array(
                    "year"=> $yr,
                    "id" => intval($arrs[$y]['Dog'][$id]['Reference']),
                    "name" => ucwords(strtolower($arrs[$y]['Dog'][$id]['Animal_Name'])),
                    "breed"=> ucwords(strtolower($arrs[$y]['Dog'][$id]['Breed_Description'])),
                    "gender"=>ucwords(strtolower($arrs[$y]['Dog'][$id]['Gender'])),
                    "status"=>ucwords(strtolower($arrs[$y]['Dog'][$id]['Status_Description'])),
                    "suburb"=>ucwords(strtolower($arrs[$y]['Dog'][$id]['Suburb'])),
                );
            }catch (ErrorException $ex){
                break;
            }
        }



        if($request->order!=null){
            $pieces = explode(" ", $request->order);
            $field = $pieces[0];
            define('field', $field);
            $type = "asc";
            if(count($pieces) ==2){
                $type = $pieces[1];
            }



            if($type=="DESC"){
                $ret = array_reverse(array_sort($ret, function ($value) {
                    return $value[field];
                }));
            }
            else{
                $ret = array_values(array_sort($ret, function ($value) {
                    return $value[field];
                }));

            }
        }

        // $ret = array_values(array_sort($ret, function ($value) {
        //     return $value['name'];
        // }));

        return response()->json($ret);

    }

    private function cmp( $a, $b){
        if( !isset( $a['age']) && !isset( $b['age'])){
            return 0;
        }

        if( !isset( $a['age'])){
            return -1;
        }

        if( !isset( $b['age'])){
            return 1;
        }

        if( $a['age'] == $b['age']){
            return 0;
        }

        return (($a['age'] > $b['age']) ? 1 : -1);
    }

    public function search($dog){
        $xml2014 = simplexml_load_file('xml/dog-registrations-2014.xml');
        $json2014 = json_encode($xml2014);
        $arr2014 = json_decode($json2014);


        $xml2015 = simplexml_load_file('xml/dog-registrations-2015.xml');
        $json2015 = json_encode($xml2015);
        $arr2015 = json_decode($json2015);
        //dd($arr2014);
        //echo $arr2014->Dog[0]->Reference;
        $ret = array();
        for ($i=0; $i <  count($arr2014->Dog) ; $i++) {
            if(intval($dog)===intval($arr2014->Dog[$i]->Reference)){
                return response()->json(array(
                    "year"=> 2014,
                    "id" => intval($arr2014->Dog[$i]->Reference),
                    "name" => ucwords(strtolower($arr2014->Dog[$i]->Animal_Name)),
                    "breed"=> ucwords(strtolower($arr2014->Dog[$i]->Breed_Description)),
                    "gender"=>ucwords(strtolower($arr2014->Dog[$i]->Gender)),
                    "status"=>ucwords(strtolower($arr2014->Dog[$i]->Status_Description)),
                    "suburb"=>ucwords(strtolower($arr2014->Dog[$i]->Suburb)),
                ));
            }
        }
        for ($i=0; $i < count($arr2015->Dog) ; $i++) {
            if(intval($dog)===intval($arr2015->Dog[$i]->Reference)){
                return response()->json(array(
                    "year"=> 2015,
                    "id" => intval($arr2015->Dog[$i]->Reference),
                    "name" => ucwords(strtolower($arr2015->Dog[$i]->Animal_Name)),
                    "breed"=> ucwords(strtolower($arr2015->Dog[$i]->Breed_Description)),
                    "gender"=>ucwords(strtolower($arr2015->Dog[$i]->Gender)),
                    "status"=>ucwords(strtolower($arr2015->Dog[$i]->Status_Description)),
                    "suburb"=>ucwords(strtolower($arr2015->Dog[$i]->Suburb)),
                ));
            }
        }


        return response()->json([
            "error" => array('status' => 404, 'message' => 'No dog registration found for \''.$dog.'\'')
        ], 404);


    }
}
